import json 
import sys
import requests as req

def input_blog_name():
    blog_name = "enter the Tumblr blog name: "
    tumblr_blog_name = input(blog_name)
    return tumblr_blog_name

def input_post_range():
  post_range = "enter the range: "
  return input(post_range)

def check_range_in_negative(tumblr_post_range):
  if  tumblr_post_range[0].isdigit():
    return tumblr_post_range
  else:
    print("Please provide valid range.")
    sys.exit()

def calc_min_max_range(tumblr_post_range):
    range=tumblr_post_range.split('-')
    #fetching the upper and lower bound for posts
    min_range = int(range[0])
    max_range = int(range[1])
    api_parameters(min_range,max_range)
      
def api_parameters(min_range,max_range):
    #num_of_posts = max_range - min_range + 1   
    api_param = {
    'type':'photo',
    'num': max_range - min_range + 1,
    'start':min_range-1
    }
    return api_param

def calc_tumblr_blog_url(tumblr_blog_name,api_param):
    tumblr_blog_url = "https://"+tumblr_blog_name+".tumblr.com/api/read/json/"
    return tumblr_blog_url

def check_url_validity(tumblr_blog_url,api_param):
    response_code = req.get(tumblr_blog_url,api_param)
    if response_code.status_code !=200:
      print("No such tumblr blog exists!!")
      sys.exit()
    return response_code