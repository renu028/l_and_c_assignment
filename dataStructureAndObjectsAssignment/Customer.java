// Creating a new Customer Class such that instead of getWallet() we are concerned about the payment, for which I have added a getPayment() method.

public class Customer {
	private String firstName;
	private String lastName;
	private Wallet myWallet = new Wallet();
	public String getFirstName(){
		return firstName;
		}
 	public String getLastName(){
 		return lastName;
 		}
 	public double getPayment(double payableAmount) {
		String paymentSuccessfulMessage = "Payment completed Successfully";
		String notSufficientBalanceMessage = "Sorry, I do not have enough money in my Wallet now";
		try{
			if(myWallet != null) {
 				if(myWallet.getTotalMoney() >= payableAmount) {
					myWallet.subtractMoney(payableAmount);
					System.out.println(paymentSuccessfulMessage);
 					return payableAmount;
					}
				else{
					throw new emptyWalletException(notSufficientBalanceMessage);
					}	 
			 	}
			 }
		catch (emptyWalletException exception) {
			System.out.println(notSufficientBalanceMessage);
			}
		return 0;
	}
	}

class emptyWalletException extends Exception{
	public emptyWalletException(String exceptionMessage){
		super(exceptionMessage);
	}
}
