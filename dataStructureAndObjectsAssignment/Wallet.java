// The class Wallet can be same, as for Wallet, the behaviour is still same, i.e., setTotalMoney() getTotalMoney() and addMoney() and after payment, subtractMoney().

public class Wallet {
	double value = 10.00;
	public double getTotalMoney() {
		return value;
		}
 	public void setTotalMoney(double newValue) {
 		value = newValue;
		}
 	public void addMoney(double deposit) {
		value += deposit; 
		}
 	public void subtractMoney(double debit) {
		value -= debit; 
		}
 }