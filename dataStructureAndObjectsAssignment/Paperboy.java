//The Client code for delivery boy can be modified as below:
public class Paperboy {
	public static void main(String[] args) {
		double payment = 12.00;
		String paymentSuccessfulMessage = "Payment was done successfully";
		String insufficientBalanceMessage = "No sufficient balance, come back later";
		// I want my two dollars!
		Customer myCustomer = new Customer();
 		double billingAmount = myCustomer.getPayment(payment);
 		if(billingAmount == payment) {
  			System.out.println(paymentSuccessfulMessage);
 			// payment was successfully done
			}
		else {
  			System.out.println(insufficientBalanceMessage);
 			// no sufficient balance, come back later and get my money
		}
	}
}

// Now, we can keep our wallet secure and we can handle all the transactions and payments by our own.
//That means, we have hidden our data and exposed only the behaviour.
