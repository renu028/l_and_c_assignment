//1) Given :

Class Employee {
 string name;
 int age;
 float salary;
 public :	string getName();
			void setName(string name);
          	int getAge();
	  		void setAge(int age);
	  		float getSalary();
	  		void setSalary(float salary);
 	};
 Employee employee;

//Question: Is 'employee' an object or a data structure? Why? 

//Answer :

//(i)   'employee' is a Data-Structure. This is because it's members, i.e., name, age, salary are just containers for the class which are storing values.
//(ii)  It consists of member-functions, i.e., getName(), setName(), getAge(), setAge(), getSalary() and setSalary() which no do not show any significant task.
//(iii) The member functions are also not showing any behaviour they are just setter and getter functions.
//(iv)  The Data-structure which consists of functions having getters and setters are example of Data transfer Objects(DTO), which are a type of Data Structures.
//(v)   Thus, from the above validations we can conclude that 'employee' is a Data-Structure, or more specifically a 'Data-Transfer Object' type of Data-Structure. 