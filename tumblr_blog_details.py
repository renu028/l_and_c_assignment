""" utility functions
This module contains helper functions.
These functions include taking input from user,
validating the range provided by user,
checking url validity.
"""
import sys
import pip._vendor.requests as req

def input_blog_name():
    """Input the tumblr blog name from user"""
    blog_name = "enter the Tumblr blog name: "
    tumblr_blog_name = input(blog_name)
    return tumblr_blog_name

def input_post_range():
    """Input the tumblr post range from user"""
    post_range = "enter the range: "
    return input(post_range)

def check_range_in_negative(tumblr_post_range):
    """Function to check the range provided by user is valid or not"""
    if  not tumblr_post_range[0].isdigit():
        print("Please provide valid range.")
        sys.exit()
    else:
        return tumblr_post_range
        
def calc_min_max_range(tumblr_post_range):
    """Calculates the starting and ending range for posts"""
    post_range = tumblr_post_range.split('-')
    #fetching the upper and lower bound for posts
    min_range = int(post_range[0])
    max_range = int(post_range[1])
    return min_range, max_range
      
def api_parameters(min_range, max_range):
    """Defining the api parameters for the url"""
    #num_of_posts = max_range - min_range + 1   
    api_param = {
        'type' : 'photo',
        'num' :  max_range - min_range + 1,
        'start' : min_range-1
    }
    return api_param

def calc_tumblr_blog_url(tumblr_blog_name):
    """Calculating the URL from api parameters"""
    tumblr_blog_url = "https://" + tumblr_blog_name + ".tumblr.com/api/read/json/"
    return tumblr_blog_url

def check_url_validity(tumblr_blog_url, api_param):
    """Validating whether the URL exists or not"""
    response_code = req.get(tumblr_blog_url, api_param)
    if response_code.status_code != 200:
        print("No such tumblr blog exists!!")
        sys.exit()
    return response_code
