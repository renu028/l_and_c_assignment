import account_operations
import client
import getpass

class ATMError(Exception): 
  
    # Constructor or Initializer 
    def __init__(self, value): 
        self.value = value 
  
    # __str__ is to print() the value 
    def __str__(self): 
        return(repr(self.value)) 


def validating_account_PIN(user_name, user_serial_number, existing_user_names, user_account_pins, account_balance):
	# comparing pin
	PIN_input_count = 0
	input_PIN_message = '\t\t\t\t\tPLEASE ENTER PIN: '
	invalid_PIN_message = '\t\t\t\t\tINVALID PIN'
	valid_PIN_message = '\t\t\t\t\tPIN SHOULD CONSISTS OF 4 DIGITS'
	successful_PIN_message = '\t\t\t\t\tLOGIN SUCCESFUL, CONTINUE'
	while PIN_input_count < 3:
		print('\t\t\t\t\t------------------')
		input_pin = str(getpass.getpass(input_PIN_message))
		print('\t\t\t\t\t------------------')
		if client.connecting_to_server():
				if input_pin.isdigit():
						if user_name == 'renu':
								if input_pin == user_account_pins[0]:
										break
								else:
										PIN_input_count += 1
										print('\t\t\t\t\t-----------')
										print(invalid_PIN_message)
										print('\t\t\t\t\t-----------')
										print()
						if user_name == 'neha':
								if input_pin == user_account_pins[1]:
										break
								else:
										PIN_input_count += 1
										print('\t\t\t\t\t-----------')
										print(invalid_PIN_message)
										print('\t\t\t\t\t-----------')
										print()
						if user_name == 'heena':
								if input_pin == user_account_pins[2]:
										break
								else:
										PIN_input_count += 1
										print('\t\t\t\t\t-----------')
										print(invalid_PIN_message)
										print('\t\t\t\t\t-----------')
										print()
						if user_name == 'yashshavi':
								if input_pin == user_account_pins[3]:
										break
								else:
										PIN_input_count += 1
										print('\t\t\t\t\t-----------')
										print(invalid_PIN_message)
										print('\t\t\t\t\t-----------')
										print()
						if user_name == 'anurag':
								if input_pin == user_account_pins[4]:
										break
								else:
										PIN_input_count += 1
										print('\t\t\t\t\t-----------')
										print(invalid_PIN_message)
										print('\t\t\t\t\t-----------')
										print()
				else:
						print('\t\t\t\t\t------------------------')
						print(valid_PIN_message)
						print('\t\t\t\t\t------------------------')
						PIN_input_count += 1
	# in case of a valid pin-continuing, or exiting
	if PIN_input_count >= 3:
			try:
				raise(ATMError('3 UNSUCCESFUL PIN ATTEMPTS,\n\t\t\t\t\tEXITING!!!!!YOUR CARD HAS BEEN LOCKED!!!!!'))
			# Value of Exception is stored in error
			except ATMError as PINAttemptError:
				print('\t\t\t\t\tA New Exception occured:',PINAttemptError.value)
				exit()

		
	print('\t\t\t\t\t-------------------------')
	print(successful_PIN_message)
	print('\t\t\t\t\t-------------------------')
	print()
	print('\t\t\t\t\t--------------------------')
	print('\t\t\t\t\t',str.capitalize(existing_user_names[user_serial_number]), 'Welcome to ATM')
	print('\t\t\t\t\t----------ATM SYSTEM-----------')
	account_operations.account_operations(user_name, user_serial_number, existing_user_names, user_account_pins, account_balance)

