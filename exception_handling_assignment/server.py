import socket                

successfulSocketCreationMessage = "\t\t\t\t\tSocket successfully created"
socketListeningMessage = "\t\t\t\t\tSocket is listening"
recievedConnectionMessage = "\t\t\t\t\tGot connection from"
serverConnectionErrorMessage = "\t\t\t\t\tError while Connecting to Server"

# next create a socket object 
mySocket = socket.socket()

print(successfulSocketCreationMessage)
  
# reserve a port on your computer in our 
# case it is 8085 but it can be anything 
port = 8085           
  
# Next bind to the port 
mySocket.bind(('', port))         
print("\t\t\t\t\tsocket binded to %s" %(port))
  
# put the socket into listening mode 
mySocket.listen(5)      
print(socketListeningMessage)            
  
# Establish connection with client. 
connection, clientAddr = mySocket.accept()      
print(recievedConnectionMessage, clientAddr)
connection.close()
