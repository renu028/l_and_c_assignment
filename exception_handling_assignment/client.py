import socket
import sys
import main

def connecting_to_server():
    # Create a socket object
    my_socket = socket.socket()
    connection_refused_error_message = "\t\t\t\t\tA New exception Occurrd: Error while connecting to Server"
    # Define the port on which you want to connect
    port = 8085
    main.main()

    try:
        # connect to the server on local computer
        my_socket.connect(('127.0.0.1', port))
        # receive data from the server
        print(my_socket.recv(1024))
        # close the connection
        my_socket.close()
        return True

    except ConnectionRefusedError:
        print(connection_refused_error_message)
        sys.exit()
