import validate_PIN

def account_details():
	existing_user_names = ['renu', 'neha', 'heena', 'yashshavi', 'anurag']
	user_account_pins = ['1111', '2222', '3333', '4444', '5555']
	account_balance = [5000, 5000, 5000, 5000, 5000]
	checking_user_account_existance(existing_user_names, user_account_pins, account_balance)

def checking_user_account_existance(existing_user_names, user_account_pins, account_balance):
	# while loop checks existance of the entered username
	input_user_name_message = '\t\t\t\t\tENTER USER NAME:'
	invalid_username_message = '\t\t\t\t\tINVALID USERNAME'
	while True:
		user_name = input(input_user_name_message)
		user_name = user_name.lower()
		if user_name in existing_user_names:
			if user_name == existing_user_names[0]:
				user_serial_number = 0
			elif user_name == existing_user_names[1]:
				user_serial_number = 1
			elif user_name == existing_user_names[2]:
				user_serial_number = 2
			elif user_name == existing_user_names[3]:
				user_serial_number = 3
			elif user_name == existing_user_names[4]:
				user_serial_number = 4
			break
		else:
			print('\t\t\t\t\t----------------')
			print(invalid_username_message)
			print('\t\t\t\t\t----------------')
	validate_PIN.validating_account_PIN(user_name, user_serial_number, existing_user_names, user_account_pins, account_balance)

