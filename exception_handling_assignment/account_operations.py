import getpass
#define Python user-defined exceptions
class ATMError(Exception): 
    def __init__(self, value): 
        self.value = value 
  
    # __str__ is to print() the value 
    def __str__(self): 
        return(repr(self.value)) 

#Main menu
def account_operations(user_name, user_serial_number, existing_user_names, user_account_pins, account_balance):
	while True:
		ATM_balance = 20000
		print('\t\t\t\t\t-------------------------------')
		response = input('\t\t\t\t\tSELECT FROM FOLLOWING OPTIONS: Statement(S) Withdraw(W) Deposit(D) Change PIN(P) Quit(Q) : ').lower()
		print('\t\t\t\t\t-------------------------------')
		valid_responses = ['s', 'w', 'd', 'p', 'q']
		response= response.lower()
		if response == 's':
			print('\t\t\t\t\t---------------------------------------------')
			print('\t\t\t\t\t',str.capitalize(existing_user_names[user_serial_number]), 'YOU HAVE ', account_balance[user_serial_number],'RUPEES IN YOUR ACCOUNT.')
			print('\t\t\t\t\t---------------------------------------------')
		elif response == 'w':
			print('\t\t\t\t\t---------------------------------------------')
			withdrawal_amount = int(input('\t\t\t\t\tENTER AMOUNT YOU WOULD LIKE TO WITHDRAW: '))
			print('\t\t\t\t\t---------------------------------------------')
			if withdrawal_amount > ATM_balance:
				try:
					raise(ATMError("INSUFFICIENT BALANCE IN ATM!!!"))
					# Value of Exception is stored in error
				except ATMError as InsufficientBalanceInATMError:
					print('\t\t\t\t\tA New Exception occured:',InsufficientBalanceInATMError.value)
				exit()
			if withdrawal_amount %10!= 0:
				try:
					raise(ATMError("AMOUNT YOU WANT TO WITHDRAW MUST MATCH 10 RUPEE NOTES!!!"))
				# Value of Exception is stored in error
				except ATMError as IncorrectDenominationError:
					print('\t\t\t\t\tA New Exception occured:',IncorrectDenominationError.value)
				exit()
			
			elif withdrawal_amount > account_balance[user_serial_number]:
				try:
   					raise(ATMError("YOU HAVE INSUFFICIENT BALANCE IN YOUR ACCOUNT!!!"))
   				# Value of Exception is stored in error
				except ATMError as InsufficientBalanceInAccountError:
   					print('\t\t\t\t\tA New Exception occured:', InsufficientBalanceInAccountError.value)
				exit()
			else:
				account_balance[user_serial_number] = account_balance[user_serial_number] - withdrawal_amount
				print('\t\t\t\t\t-----------------------------------')
				print('\t\t\t\t\tYOUR NEW BALANCE IS: ', account_balance[user_serial_number], 'RUPEES')
				print('\t\t\t\t\t-----------------------------------')
		elif response== 'd':
			print()
			print('\t\t\t\t\t---------------------------------------------')
			deposit_amount = int(input('\t\t\t\t\tENTER AMOUNT YOU WANT TO DEPOSIT:'))
			print('\t\t\t\t\t---------------------------------------------')
			print()
			if deposit_amount %10 != 0:
				print('\t\t\t\t\t----------------------------------------------------')
				print('\t\t\t\t\tAMOUNT YOU WANT TO DEPOSIT MUST MATCH 10 RUPEES NOTES')
				print('\t\t\t\t\t----------------------------------------------------')
			else:
				account_balance[user_serial_number] = account_balance[user_serial_number] + deposit_amount
				print('\t\t\t\t\t----------------------------------------')
				print('\t\t\t\t\tYOUR NEW BALANCE IS: ', account_balance[user_serial_number], 'RUPEES')
				print('\t\t\t\t\t----------------------------------------')
		elif response == 'p':
			print('\t\t\t\t\t-----------------------------')
			new_pin = str(getpass.getpass('\t\t\t\t\tENTER A NEW PIN: '))
			print('\t\t\t\t\t-----------------------------')
			if new_pin.isdigit() and new_pin != user_account_pins[user_serial_number] and len(new_pin) == 4:
				print('\t\t\t\t\t------------------')
				confirmation_pin = str(getpass.getpass('\t\t\t\t\tCONFIRMNEW PIN: '))
				print('\t\t\t\t\t-------------------')
				if confirmation_pin != new_pin:
					print('\t\t\t\t\t------------')
					print('\t\t\t\t\tPIN MISMATCH')
					print('\t\t\t\t\t------------')
				else:
					user_account_pins[user_serial_number] = new_pin
					print('\t\t\t\t\tNEW PIN SAVED')
			else:
				print('\t\t\t\t\t-------------------------------------')
				print('\t\t\t\t\tNEW PIN MUST CONSIST OF 4 DIGITS AND MUST BE DIFFERENT TO PREVIOUS PIN')
				print('\t\t\t\t\t-------------------------------------')
		elif response== 'q':
			exit()
		else:
			print('\t\t\t\t\t------------------')
			print('\t\t\t\t\tRESPONSE NOT VALID')
			print('\t\t\t\t\t------------------')

