({
        printLatLng: function(component){
        var placeNotFoundMessage = "coordinates of the place are not found";
        var emptySearchMessage = "Please enter a location";
            
        var responseValue = component.get("v.response");
        if(responseValue != null){
            var responseLength = responseValue.length;
            if(responseLength != 0){
                var lat = responseValue[0].geometry.coordinates[1];
                var lng = responseValue[0].geometry.coordinates[0];
                var map = component.get("v.MapObj");
                //setting marker on map for the specified place and attaching lat, lng to it
                var markerOnMap = L.marker([lat,lng],{
                }).addTo(map).bindPopup(lat+','+lng);
                map.setView([lat,lng], 7);
                component.set("v.place",null); 
            }
            else{
                alert(placeNotFoundMessage);
                component.set("v.place",null); 
            } 
        }
        else{
            alert(emptySearchMessage);
            component.set("v.place",null); 
        }		
	}
})
