({
    jsLoaded: function(component) {
        //function to load map
        setTimeout(function() {
            // to create a map with Indore co-ordinates(centre) and zoom level 5
            var map = L.map('map', {zoomControl: false}).setView([22.719568, 75.857727], 5);
            L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}',
                        {
                            //map-name provided by Leaflet
                            attribution: 'Tiles © Esri'
                        }).addTo(map);      
            //setting the variable for MapObject
            component.set("v.MapObj",map);
        });    
    },
    
    handleClick: function(component, event, helper) {
        // getting place and lat lng
        var placeNotFoundMessage = "coordinates of the place are not found";
        var emptySearchMessage = "Please enter a location";
        var place = component.find("place");
        var action = component.get("c.getLatLng"); 
        if(place.get("v.value") != undefined || place.get("v.value") != null){
            action.setParams({"placeName": place.get("v.value")});   
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state == 'SUCCESS'){
                    component.set("v.response",response.getReturnValue());
                    helper.printLatLng(component);
                }
                else{
                    alert(placeNotFoundMessage);
                    component.set("v.place",null); 
                }       
            });
            $A.enqueueAction(action);
        }
        else{
            alert(emptySearchMessage);
            component.set("v.place",null); 
        }
        place.set("v.null");    
    },    
})
