""" Main driver Program
This module contains the Main Driver program along with other functions
that include fetching data from tumblr api and printing basic blog details
and photo urls of posts that might also contain multiple photos in a
single post.    
"""
import json
import pip._vendor.requests as req
from tumblr_blog_details import input_blog_name, input_post_range, check_range_in_negative 
from tumblr_blog_details import calc_min_max_range, api_parameters
from tumblr_blog_details import calc_tumblr_blog_url, check_url_validity

def fetch_json_data_from_tumblr_url(response_code):
    """Fetches data in json format from url"""
    fetched_string_result = response_code.text[22:-2] 
    #data fetched as string  
    json_parsed_data = json.loads(fetched_string_result)  
    #data is converted into json object
    return json_parsed_data
     
def print_basic_blog_details(json_parsed_data):
    """Prints basic blog details like name,total no. of posts, title and decription"""
    print("name:", json_parsed_data["tumblelog"]["name"])
    print("Total:", json_parsed_data["posts-total"])
    print("title:", json_parsed_data["tumblelog"]["title"])
    print("description:", json_parsed_data["tumblelog"]["description"])

def print_photo_post_urls(tumblr_blog_url, json_parsed_data, tumblr_post_range):
    """Prints photo urls for posts having one or more photos in them"""
    post_range = tumblr_post_range.split('-')
    post_counter = int(post_range[0])
    max_range = int(post_range[1])
    while post_counter < max_range:
        api_param = api_parameters(post_counter, max_range)
        response_code = req.get(tumblr_blog_url, api_param)
        json_parsed_data = fetch_json_data_from_tumblr_url(response_code)
        for post in json_parsed_data["posts"]:
            print(str(post_counter) + ". " + post["photo-url-1280"])
            if post["photos"] == 0:
                #check if multiple posts exist or not in a post
                pass
            else:
                for multiple_post in post["photos"]:
                    print("    " + multiple_post["photo-url-1280"])
            post_counter += 1
      
def main():
    """Main function calling all other functions"""
    tumblr_blog_name = input_blog_name()   
    tumblr_post_range = input_post_range()
    tumblr_post_range = check_range_in_negative(tumblr_post_range)
    min_range, max_range = calc_min_max_range(tumblr_post_range)
    api_param = api_parameters(min_range, max_range)
    tumblr_blog_url = calc_tumblr_blog_url(tumblr_blog_name)
    response_code = check_url_validity(tumblr_blog_url, api_param)
    json_parsed_data = fetch_json_data_from_tumblr_url(response_code)
    print_basic_blog_details(json_parsed_data)
    print_photo_post_urls(tumblr_blog_url, json_parsed_data, tumblr_post_range)

main()
